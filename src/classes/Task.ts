import { Status } from './Status';
import { User } from './User';

export class Task {
	id: number = 0;
	title: string = '';
	description: string = '';
	status: Status;
	user: User;

	constructor(title: string, description: string, status: Status, user: User, id: number = new Date().getTime()) {
		this.id = id;
		this.title = title;
		this.description = description;
		this.status = status;
		this.user = user;
	}
}
