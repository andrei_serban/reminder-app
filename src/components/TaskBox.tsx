import * as React from "react";

import { Task } from '../classes/Task';
import { User } from '../classes/User';
import { Status } from '../classes/Status';

interface Props {
  task: Task,
  users: User[],
  statuses: Status[],
  onTaskUpdate(event: any): void,
  onTaskDelete(task: Task): void
};

interface State {
  edit: boolean
}

interface Event {
  target: any
}

export default class TaskBox extends React.Component<Props, State> {
  constructor(props: any) {
    super(props);

    this.state = {
      edit: false
    };
  }

  onEditTrigger = () => {
    this.setState({ edit: !this.state.edit });
  }

  onTaskUpdate = (event: Event) => {
    const { task, users, statuses, onTaskUpdate } = this.props;
    const { name, value } = event.target;

    if ( name === 'title' ) {
      task.title = value;
    } else if ( name === 'description' ) {
      task.description = value;
    } else if ( name === 'status' ) {
      const status = statuses.find(s => s.id === value);
      
      if ( status ) {
        task.status = status;
      }
    } else if ( name === 'user' ) {
      const user = users.find(u => u.id === value);

      if ( user ) {
        task.user = user;
      }
    }

    onTaskUpdate(task);
  }

  onTaskDelete = () => {
    const { task, onTaskDelete } = this.props;

    onTaskDelete(task);
  }

  render() {
    const { edit } = this.state;
    const { task, users, statuses } = this.props;

    return (
      <div className="panel panel-default">
        <div className="panel-heading">
          {
            edit 
            ? <input type="text" className="form-control" value={ task.title } onChange={ this.onTaskUpdate } name="title" />
            : <h3 className="panel-title">{ task.title }</h3> 
          }
        </div>
        <div className="panel-body">
          {
            edit 
            ? <textarea className="form-control" value={ task.description } onChange={ this.onTaskUpdate } name="description"></textarea>
            : <div>{ task.description }</div>
          }
        </div>
        <div className="panel-footer">
          <div className="row">
            <div className="col col-md-4">
              <label>Status:</label>
              {
                edit 
                ? <select className="form-control" name="status" value={ task.status.id } onChange={ this.onTaskUpdate }>
                  {statuses.map((status, i) => <option key={i} value={ status.id }>{ status.title }</option>)}
                </select>
                : <div>{ task.status ? task.status.title : '-' }</div>
              }
            </div>
            <div className="col col-md-4">
              <label>User:</label>
              {
                edit 
                ? <select className="form-control" name="user" value={ task.user.id } onChange={ this.onTaskUpdate }>
                  {users.map((user, i) => <option key={i} value={ user.id }>{ user.name }</option>)}
                </select>
                : <div>{ task.user ? task.user.name : '-' }</div>
              }
            </div>
            <div className="col col-md-4">
              <button className="btn btn-primary btn-block" onClick={ this.onEditTrigger }>
                <i className="fa fa-edit"></i>
              </button>
              <button className="btn btn-danger btn-block" onClick={ this.onTaskDelete }>
                <i className="fa fa-trash"></i>
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
