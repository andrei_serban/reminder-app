import * as React from "react";

import TaskBox from './TaskBox';

import { Task } from '../classes/Task';
import { User } from '../classes/User';
import { Status } from '../classes/Status';

interface Props {
	tasks: Task[],
	users: User[],
	statuses: Status[],
	onTaskUpdate(event: any): void,
	onTaskDelete(task: Task): void
};

export default class Column extends React.Component<Props, any> {
	render() {
		const { tasks, users, statuses, onTaskUpdate, onTaskDelete } = this.props;

		return (
	    	<div>
	    		{tasks.map((task, i) => <TaskBox key={i} 
					    						task={task} 
					    						users={users} 
					    						statuses={statuses}
					    						onTaskUpdate={onTaskUpdate} 
					    						onTaskDelete={onTaskDelete} />
	          	)}
  			</div>
	  	);
	}
}
