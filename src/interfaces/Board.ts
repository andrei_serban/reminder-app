import { Task } from '../classes/Task';

export interface Board {
    title: string,
    status: string,
	tasks: Task[]
}
