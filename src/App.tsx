import * as React from 'react';
import './App.css';

import { Task } from './classes/Task';
import { User } from './classes/User';
import { Status } from './classes/Status';

import { Board } from './interfaces/Board';

import Column from './components/Column';

interface State {
  users: User[],
  statuses: Status[],
  boards: Board[]
};

export default class App extends React.Component<{}, State> {
  constructor(props: any) {
    super(props);

    this.state = {
      users: [],
      statuses: [],
      boards: [
        {
          title: 'New',
          status: 'new',
          tasks: []
        },
        {
          title: 'In Progress',
          status: 'progress',
          tasks: []
        },
        {
          title: 'QA',
          status: 'qa',
          tasks: []
        },
        {
          title: 'Completed',
          status: 'done',
          tasks: []
        }
      ]
    };
  }

  componentDidMount() {
    const { boards, statuses, users } = this.state;

    for ( let i in boards ) {
      statuses.push( new Status(boards[i].status, boards[i].title) );
    }

    users.push( new User('andrei', 'Andrei Serban', 'andrei@email.com') );
    users.push( new User('mark', 'Mark Stone', 'mark@email.com') );
    users.push( new User('john', 'John Francis', 'john@email.com') );
    
    boards[0].tasks.push( new Task('Build website', 'Follow the design provided and the written specs..', statuses[0], users[1], 1) );
    boards[0].tasks.push( new Task('Deploy website', 'Buy a DigitalOcean server first.', statuses[0], users[2], 2) );

    boards[1].tasks.push( new Task('Send email', 'To the whole team.', statuses[1], users[0], 3) );

    boards[2].tasks.push( new Task('Write specs', 'At least 5 pages.', statuses[2], users[0], 4) );

    boards[3].tasks.push( new Task('Design flow', 'In Zeplin.', statuses[3], users[0], 5) );
    
    this.setState({  
      users: users,
      statuses: statuses,
      boards: boards
    });
  }

  onTaskCreate = () => {
    const { boards, statuses, users } = this.state;

    boards[0].tasks.push( new Task('', '', statuses[0], users[0]) );

    this.setState({ boards });
  }

  onTaskUpdate = (task: Task) => {
    const { boards } = this.state;

    for ( let i in boards ) {
      if ( boards[i].status === task.status.id ) {
        const currentTask = boards[i].tasks.find(t => t.id == task.id);

        //console.log('currentTask', currentTask, boards[i].tasks, task.id);

        if ( currentTask ) {
          console.log( currentTask.status.id, task.status.id );
          if ( currentTask.status.id === task.status.id ) {
            boards[i].tasks[ boards[i].tasks.findIndex(t => t.id === task.id) ] = task;
          } else {
            console.log('status change');
          }
        }

        break;
      }
    }
    
    this.setState({ boards });
  }

  onTaskDelete = (task: Task) => {
    if ( window.confirm('Are you sure?') ) {

      const { boards } = this.state;

      for ( let i in boards ) {
        if ( boards[i].status === task.status.id ) {
          boards[i].tasks.splice( boards[i].tasks.findIndex(t => t.id === task.id), 1 );
          break;
        }
      }
      
      this.setState({ boards });
    }
  }

  render() {
    const { boards, statuses, users } = this.state;

    return (
      <div className="App">
        <div className="row">
          {boards.map((board, i) => 
            <div className="col col-md-3" key={i}>
              <h3 className="column-title">{ board.title }</h3>
              {
                board.status === 'new'
                ? <button className="btn btn-primary btn-block btn-add" onClick={ this.onTaskCreate }>Add new</button>
                : null
              }
              <Column tasks={ board.tasks } 
                      users={ users } 
                      statuses={ statuses }
                      onTaskUpdate={ this.onTaskUpdate } 
                      onTaskDelete={ this.onTaskDelete } />
            </div>
          )}
        </div>
      </div>
    );
  }
}
